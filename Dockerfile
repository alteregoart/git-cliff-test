FROM rust:1.54 as builder

RUN cargo install git-cliff

FROM node:12

COPY --from=builder /usr/local/cargo/bin /usr/local/bin

ENV WORKING_DIR="/home/node/app"

RUN mkdir -p $WORKING_DIR/node_modules && chown -R node $WORKING_DIR

WORKDIR $WORKING_DIR

USER node

RUN npm i jsdoc docdash redoc-cli markdown-folder-to-html
