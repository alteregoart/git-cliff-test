# git cliff test

## Update the docker image

The continuous integration for the documentation pages in this project makes use of a pre-built docker image called **node-documentation** stored in [GitLab's container registry](https://gitlab.com/alteregoart/git-cliff-test/container_registry). It is built on top of **node:12** and installs the necessary packages.

To update it, you can modify the two docker config files, _Dockerfile_

To use docker locally, you need to have docker installed on your machine (see [this link for details](https://docs.docker.com/get-docker/)), and log in using the `docker login` command.

```
docker login -u USERNAME -p PASSWORD
```

Once docker is correctly installed and configured on your machine, launch docker,and run these two commands:
```
docker build -t registry.gitlab.com/alteregoart/git-cliff-test/node-documentation .
docker push registry.gitlab.com/alteregoart/git-cliff-test/node-documentation
```

This will create the image and update it in the registry. You can now use it in this project by specifying `image: registry.gitlab.com/alteregoart/git-cliff-test/node-documentation:latest` !

Add a new commit
